# vue-todo

Todo 앱을 만들어 봅시다.

## Instructions

### 1. Vue Project 생성하기

우선 Vue CLI를 사용해 Vue.js 프로젝트를 생성합니다.
아래 지시사항에 따라 프로젝트를 설정합니다.
지시사항에 언급되지 않은 것은 기본값으로 둡니다.

#### 1. Vue.js Project 생성 및 기본 설정

* 패키지 매니저는 npm을 사용합니다.
* Git 저장소는 만들지 않습니다.
* Preset을 사용하지 않고 직접 설정합니다.
* 최신 JavaScript 기능을 지원하지 않는 브라우저에서도 잘 동작할 수 있도록 하기 위해 Babel을 사용합니다.
* Linter / Formatter를 사용합니다.
* `package.json` 대신 `.babelrc`와 같이 각각의 설정 파일을 사용하도록 합니다.
* Linter는 ESLint를 사용하며 Airbnb 스타일을 따릅니다.

#### 2. Dependency 설치

프로젝트에서 사용할 dependency를 설치하고 설정합니다.

* Vuetify를 사용합니다(plugin).
  * Preset을 사용하지 않고 직접 설정합니다.
  * Pre-made template을 사용하지 않습니다. 대신 주어진 코드를 사용할 것입니다.
  * Locale은 한국으로 설정합니다.

### 2. 베이스 코드 붙여넣기

기본적인 UI가 구현된 코드가 제공되었습니다.
제공된 소스 파일을 프로젝트 내의 적절한 위치로 옮깁니다.

### 3. 기능 구현해 프로젝트 완성하기

빠진 부분에 적절한 코드를 채워넣어 아래와 기능들을 구현합니다.
\+ 로 표시된 항목은 제공된 코드에 이미 구현되어 있는 것, \- 로 표시된 항목은 여러분이 구현해야 할 것입니다.\
구현해야 하는 부분은 제공된 소스 파일에 TODO 주석(`<!-- TODO: #1-1 -->` 또는 `/* TODO: #2-2 */`)으로 표시되어 있습니다.
TODO 주석의 위치에 번호에 맞는 기능을 구현하기 위한 코드를 작성하면 됩니다.
예를 들어 `<!-- TODO: #1-1 -->` 주석으로 표시된 위치들에는 1-1번 기능을 구현하기 위한 코드를 작성하면 됩니다.
구현이 완료되면 TODO 주석은 지우세요.

#### 1. `UserName` Component

##### 1. 사용자 이름 표시 기능

\+ `UserName` component는 사용자 이름을 저장하기 위한 변수 `userName`을 가집니다.
기본값은 빈 문자열(`''`)입니다.\
\- `UserName` component가 변수 `userName`에 저장된 사용자 이름을 표시하도록 하세요.\
\- 사용자 이름이 설정되지 않은 경우(빈 문자열인 경우) "Click to set name"이 표시되도록 하세요.

##### 2. 사용자 이름 설정 및 수정 기능

\+ `UserName` component는 사용자 이름을 설정하기 위한 input의 값을 나타내는 변수 `userNameInput`을 가집니다.\
\+ `UserName` component는 dialog의 활성화 여부를 나타내는 변수 `dialog`를 가집니다.
이 변수의 값이 `true`이면 dialog가 표시되고, `false`이면 표시되지 않습니다.
따라서 dialog를 보여주려면 이 값을 `true`로 만들면 되고, 닫으려면 `false`로 만들면 됩니다.\
\+ `UserName` component를 클릭하면 이름을 설정 또는 수정할 수 있는 dialog가 표시됩니다.\
\- [Save] 버튼을 클릭하면 `setName()` method를 호출하도록 하세요.\
\- 사용자 이름 변수 `userName`의 값을 input에 입력한 값으로 설정하고 dialog를 닫도록 `setName()` method를 작성하세요.

#### 2. `TodoItem` Component

하나의 할 일은 dictionary 하나로 표현됩니다.
할 일 dictionary는 등록된 순서를 나타내는 `idx`, 할 일 제목을 나타내는 `title`, 그리고 완료 여부를 나타내는 `isDone`을 포함하고 있습니다.
예를 들어 첫 번째로 등록된 "Read a book"을 완료하였다면 아래와 같이 표현됩니다.
```JavaScript
{
  idx: 0,
  title: 'Read a book',
  isDone: true,
}
```
할 일들의 목록은 이 dictionary들의 배열로 표현됩니다.

##### 1. 할 일 표시 기능

\- `TodoItem` component가 할 일 하나를 표현하는 dictionary를 props `todo`로 전달받도록 하세요.\
\- `TodoItem` component가 props로 전달받은 `todo`의 title을 출력하도록 하세요.\
\+ `TodoItem` component에는 할 일의 완료 여부를 표시하는 checkbox가 있습니다.\
\- 이 checkbox에 props로 전달받은 `todo`의 완료 여부가 표시되도록 하세요.
이 checkbox를 클릭해 값을 수정하면 props로 전달받은 `todo`의 완료 여부 값도 바뀌어야 합니다(양방향 바인딩).

#### 3. `TodoList` Component

##### 1. 할 일 목록 표시 기능

\+ `TodoList` component는 할 일 목록을 저장할 수 있는 변수 `todoList`를 가집니다.
이 변수는 앞서 설명한 할 일을 나타내는 dictionary들의 배열입니다.\
\+ 할 일 목록 변수 `todoList`의 기본값은 빈 배열(`[]`) 입니다.\
\- `TodoList` component가 할 일 목록에 저장된 할 일 목록을 표시하도록 하세요.\
\- 할 일 목록에 할 일이 없는 경우(배열이 빈 경우) "There is nothing to do"가 표시되도록 하세요.\
\+ 각각의 할 일은 제공된 코드의 `TodoItem` component를 사용하여 표시합니다.\
\- `TodoItem` component가 props를 전달받도록 했으므로 `todoList`의 item들을 props로 전달하면 됩니다.

##### 2. 할 일 추가 기능

\+ `TodoList` component에는 할 일을 입력하기 위한 input이 있습니다.\
\+ `TodoList` component는 할 일을 입력하기 위한 input의 값을 나타내는 변수 `todoInput`을 가집니다.\
\- [Enter] 키를 누르면 `addTodo()` method를 호출하도록 하세요.\
\- 할 일 목록 변수 `todoList`에 입력한 할 일을 추가하도록 `addTodo()` method를 작성하세요.
추가한 후 input은 빈 상태로 돌아가야 합니다.

##### 3. Local Storage에 저장하고 불러오기

\+ Local storage에 현재 todo list 목록을 `saveAll()` method와 저장된 todo list 목록을 불러오는 `loadAll()` method가 구현되어 있습니다.\
\- 애플리케이션이 실행되었을 때 `loadAll()`을 호출하여 저장된 할 일 목록을 불러오도록 하세요.\
\- 할 일 목록 변수 `todoList`가 변경되었을 때 `saveAll()`을 호출하여 현재 할 일 목록을 저장하도록 하세요.

### 4. Vuex 사용하도록 수정하기

#### 1. Dependency 설치

Vuex를 사용하기 위해 dependency를 추가로 설치합니다.

* Vuex를 사용합니다(plugin).

#### 2. Vuex를 사용하도록 수정하기

##### 1. 사용자 이름

사용자 이름이 Vuex를 통해 관리되도록 수정합니다.

\- 사용자 이름을 저장하는 `userName` state를 Vuex store에 추가하세요.\
\- 사용자 이름이 설정된 경우 사용자 이름을 반환하고, 설정되지 않은 경우(빈 문자열인 경우) "Click to set name"을 반환하는 getter를 Vuex store에 추가하세요.\
\- 사용자 이름을 설정하는 mutation을 Vuex store에 추가하세요.\
\- 추가한 getter와 mutation을 사용하도록 component를 수정하세요.

##### 2. 할 일 목록

할 일 목록이 Vuex를 통해 관리되도록 수정합니다.

\- 할 일 목록을 저장하는 `todoList` state를 Vuex store에 추가하세요.\
\- 할 일 목록에 할 일을 추가하는 mutation을 Vuex store에 추가하세요.\
\- Local storage에 저장된 할 일 목록을 불러오는 mutation을 Vuex store에 추가하세요.\
\- 추가한 state와 mutation을 사용하도록 component를 수정하세요.

### 5. 메인 페이지 추가하기

#### 1. Dependency 설치

##### 1. Vue Router 설치

\- 페이지 이동을 위해 Vue Router를 설치합니다.

##### 2. `App.vue` 파일 수정

\+ Vue Router를 설치하는 경우 `App.vue` 파일이 제멋대로 변경됩니다.\
\- 변경된 파일의 내용을 주어진 `App.vue` 파일의 내용으로 변경합니다.

```Vue
<template>
  <v-app>
    <v-app-bar
      app
      color="primary"
      dark
    >
      <v-toolbar-title>MyTODO</v-toolbar-title>

      <v-spacer></v-spacer>
      <user-name></user-name>
    </v-app-bar>
    <v-content>
      <v-container>
        <v-layout>
          <v-flex
            xs12
          >
            <!-- TODO: #2-1 -->

          </v-flex>
        </v-layout>
      </v-container>
    </v-content>
  </v-app>
</template>

<script>
import UserName from '@/components/UserName.vue';

export default {
  name: 'App',

  components: {
    UserName,
  },
};
</script>
```

##### 3. View 파일 삭제

\+ Vue Router를 설치하는 경우 `@/views/Home.vue` 파일과 `@/views/About.vue` 파일이 생성됩니다.\
\- 두 파일을 모두 삭제합니다.

#### 2. Vue Router 설정

##### 1. Router View 추가

\- `App.vue`에 router view가 표시되도록 router view component를 추가하세요.

##### 2. View 파일 추가

\- 주어진 `Home.vue` 파일을 `@/views/` directory로 옮깁니다.\
\- 전에 작업했던 `@/components/TodoList.vue` 파일을 `@/views/` directory로 옮깁니다.

##### 3. Route 설정

\- `@/router/index.js` 파일에 두 개의 route를 추가합니다.\
\- 첫 번째 route의 경로는 `/`, 이름은 `home`, 보여져야 하는 view는 `@/views/Home.vue` 입니다.\
\+ `Home.vue` 파일은 주어진 파일을 사용합니다.\
\- 두 번째 route의 경로는 `/todo`, 이름은 `todo`, 보여져야 하는 view는 `@/views/TodoList.vue` 입니다.

#### 3. 메인 페이지 기능 추가

주어진 `@/views/Home.vue` 파일에 다음 기능을 구현합니다.

##### 1. 남은 todo 개수 보여주기

\- 현재 등록된 todo 중 아직 완료하지 않은 것의 개수를 보여줍니다.\
\- 구현 방식은 vuex store에 getter를 추가해도 되고, component에 computed를 추가해도 됩니다.
자유롭게 구현하세요.\
\- 출력 형식은 'You have 0 tasks to do.'로 합니다.

##### 2. 버튼 클릭시 todo view로 이동시키기

\- Todo list view로 이동시키는 method `showTodoList()`를 구현합니다.\
\- Show todo list 버튼을 클릭하면 `showTodoList()` method를 호출하도록 합니다.

#### 4. 그 외 수정사항

##### 1. 애플리케이션이 실행될 때 저장한 데이터를 로드하도록 수정

\+ 현재 코드는 todo view가 create될 때 데이터를 로드합니다.\
\- 애플리케이션이 실행될 때 데이터를 로드하도록 하기 위해 `App.vue`가 create될 때 데이터를 로드하도록 수정합니다.
