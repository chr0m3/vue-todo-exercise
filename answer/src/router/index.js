import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/views/Home.vue';

Vue.use(VueRouter);

const routes = [
  /* TODO: #2-3 */
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/todo',
    name: 'todo',
    component: () => import('@/views/TodoList.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
