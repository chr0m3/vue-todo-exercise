import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const STORAGE_KEY = 'mytodo';

export default new Vuex.Store({
  state: {
    userName: '',
    idx: 0,
    todoList: [],
  },
  getters: {
    displayUserName(state) {
      return state.userName ? state.userName : 'Click here to set name';
    },
    remainTodoCount(state) {
      let count = 0;
      // eslint-disable-next-line no-restricted-syntax
      for (const todo of state.todoList) if (todo.isDone === false) count += 1;
      return count;
    },
  },
  mutations: {
    setName(state, value) {
      state.userName = value;
    },
    loadAll(state) {
      state.todoList = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
      state.todoList.forEach((todo, index) => {
        // eslint-disable-next-line no-param-reassign
        todo.idx = index;
      });
      state.idx = state.todoList.length;
    },
    saveAll(state) {
      localStorage.setItem(STORAGE_KEY, JSON.stringify(state.todoList));
    },
    addTodo(state, value) {
      state.todoList.push({
        idx: state.idx,
        title: value,
        isDone: false,
      });
    },
  },
});
